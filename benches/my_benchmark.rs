use aoc_2020::*;
use criterion::{black_box, criterion_group, criterion_main, Criterion};
use std::fs;

fn criterion_benchmark(c: &mut Criterion) {
    //Day01
    let raw_input = fs::read_to_string("inputs/day01/problem.txt").unwrap();
    let input = day01::parse_input(&raw_input);
    c.bench_function("Day01:Parse", |b| {
        b.iter(|| {
            let _test = day01::parse_input(&raw_input);
        })
    });
    c.bench_function("Day01:P1", |b| {
        b.iter(|| day01::part_one(black_box(&input)))
    });
    c.bench_function("Day01:P2", |b| {
        b.iter(|| day01::part_two(black_box(&input)))
    });
    //Day02
    let raw_input = fs::read_to_string("inputs/day02/problem.txt").unwrap();
    let input = day02::parse_input(&raw_input);
    c.bench_function("Day02:Parse", |b| {
        b.iter(|| {
            let _test = day02::parse_input(&raw_input);
        })
    });
    c.bench_function("Day02:P1", |b| {
        b.iter(|| day02::part_one(black_box(&input)))
    });
    c.bench_function("Day02:P2", |b| {
        b.iter(|| day02::part_two(black_box(&input)))
    });
    //Day03
    let raw_input = fs::read_to_string("inputs/day03/problem.txt").unwrap();
    let input = day03::parse_input(&raw_input);
    c.bench_function("Day03:Parse", |b| {
        b.iter(|| {
            let _test = day03::parse_input(&raw_input);
        })
    });
    c.bench_function("Day03:P1", |b| {
        b.iter(|| day03::part_one(black_box(&input)))
    });
    c.bench_function("Day03:P2", |b| {
        b.iter(|| day03::part_two(black_box(&input)))
    });
    //Day04
    let raw_input = fs::read_to_string("inputs/day04/problem.txt").unwrap();
    let input = day04::parse_input(&raw_input);
    c.bench_function("Day04:Parse", |b| {
        b.iter(|| {
            let _test = day04::parse_input(&raw_input);
        })
    });
    c.bench_function("Day04:P1", |b| {
        b.iter(|| day04::part_one(black_box(&input)))
    });
    c.bench_function("Day04:P2", |b| {
        b.iter(|| day04::part_two(black_box(&input)))
    });
    //Day05
    let raw_input = fs::read_to_string("inputs/day05/problem.txt").unwrap();
    let input = day05::parse_input(&raw_input);
    c.bench_function("Day05:Parse", |b| {
        b.iter(|| {
            let _test = day05::parse_input(&raw_input);
        })
    });
    c.bench_function("Day05:P1", |b| {
        b.iter(|| day05::part_one(black_box(&input)))
    });
    c.bench_function("Day05:P2", |b| {
        b.iter(|| day05::part_two(black_box(&input)))
    });
    //Day06
    let raw_input = fs::read_to_string("inputs/day06/problem.txt").unwrap();
    let input = day06::parse_input(&raw_input);
    c.bench_function("Day06:Parse", |b| {
        b.iter(|| {
            let _test = day06::parse_input(&raw_input);
        })
    });
    c.bench_function("Day06:P1", |b| {
        b.iter(|| day06::part_one(black_box(&input)))
    });
    c.bench_function("Day06:P2", |b| {
        b.iter(|| day06::part_two(black_box(&input)))
    });
    //Day08
    let raw_input = fs::read_to_string("inputs/day08/problem.txt").unwrap();
    let input = day08::parse_input(&raw_input);
    c.bench_function("Day08:Parse", |b| {
        b.iter(|| {
            let _test = day06::parse_input(&raw_input);
        })
    });
    c.bench_function("Day08:P1", |b| {
        b.iter(|| day08::part_one(black_box(&input)))
    });
    c.bench_function("Day08:P2", |b| {
        b.iter(|| day08::part_two(black_box(&input)))
    });
    //Day09
    let raw_input = fs::read_to_string("inputs/day09/problem.txt").unwrap();
    let input = day09::parse_input(&raw_input);
    c.bench_function("Day09:Parse", |b| {
        b.iter(|| {
            let _test = day09::parse_input(&raw_input);
        })
    });
    /*
    c.bench_function("Day09:P1", |b| {
        b.iter(|| day08::part_one(black_box(&input)))
    });
    c.bench_function("Day09:P2", |b| {
        b.iter(|| day08::part_two(black_box(&input)))
    });
    */
}

criterion_group!(benches, criterion_benchmark);
criterion_main!(benches);
