use std::fs;
use std::time::Instant;

pub fn exec() {
    let raw_input = fs::read_to_string("inputs/day06/problem.txt").unwrap();
    println!("Advent of Code: 2020-12-06");
    let start = Instant::now();
    let input = parse_input(&raw_input);
    println!("P1: {}", part_one(&input));
    println!("P2: {}", part_two(&input));
    println!("Elapsed Time: {}ms\n", start.elapsed().as_millis());
}

pub fn part_one(input: &(u32, u32)) -> u32 {
    input.0
}

pub fn part_two(input: &(u32, u32)) -> u32 {
    input.1
}

pub fn parse_input(input: &str) -> (u32, u32) {
    let mut value: u32 = 0;
    let mut solution: (u32, u32) = (0, 0);
    let mut part_one: u32 = 0;
    let mut part_two: u32 = u32::MAX;

    let bytes = input.as_bytes();
    let mut index: usize = 0;
    while index < bytes.len() {
        if bytes[index] == b'\n' {
            if value == 0 {
                solution.0 += part_one.count_ones();
                solution.1 += part_two.count_ones();
                part_one = 0;
                part_two = u32::MAX;
            } else {
                part_one |= value;
                part_two &= value;
                value = 0;
            }
            index += 1;
        } else if bytes[index] == b'\r' {
            if value == 0 {
                solution.0 += part_one.count_ones();
                solution.1 += part_two.count_ones();
                part_one = 0;
                part_two = u32::MAX;
            } else {
                part_one |= value;
                part_two &= value;
                value = 0;
            }
            index += 2;
        } else {
            value |= 1 << bytes[index] - b'a';
            index += 1;
        }
    }
    part_one |= value;
    part_two &= value;
    solution.0 += part_one.count_ones();
    solution.1 += part_two.count_ones();
    solution
}

#[cfg(test)]
mod tests {
    use super::*;
    macro_rules! part_one_tests {
        ($($name:ident: $value:expr,)*) => {
        $(
            #[test]
            fn $name() {
                let (path, expected) = $value;
                let raw_input = fs::read_to_string(path).unwrap();
                let input = parse_input(&raw_input);
                assert_eq!(part_one(&input), expected);
            }
        )*
        }
    }

    macro_rules! part_two_tests {
        ($($name:ident: $value:expr,)*) => {
        $(
            #[test]
            fn $name() {
                let (path, expected) = $value;
                let raw_input = fs::read_to_string(path).unwrap();
                let input = parse_input(&raw_input);
                assert_eq!(part_two(&input), expected);
            }
        )*
        }
    }
    part_one_tests! {
        part_one_problem:   ("inputs/day06/problem.txt",    6612),
        part_one_1:         ("inputs/day06/test01.txt",     11),
    }

    part_two_tests! {
        part_two_problem:   ("inputs/day06/problem.txt",    3268),
        part_two_1:         ("inputs/day06/test01.txt",     6),
    }
}
