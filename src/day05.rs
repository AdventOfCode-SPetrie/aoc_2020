use std::fs;
use std::time::Instant;

pub fn exec() {
    let raw_input = fs::read_to_string("inputs/day05/problem.txt").unwrap();
    println!("Advent of Code: 2020-12-05");
    let start = Instant::now();
    let input: Vec<u32> = parse_input(&raw_input);
    println!("P1: {}", part_one(&input));
    println!("P2: {}", part_two(&input));
    println!("Elapsed Time: {}ms\n", start.elapsed().as_millis());
}

pub fn part_one(input: &[u32]) -> u32 {
    *input.iter().max().unwrap()
}

pub fn part_two(input: &[u32]) -> u32 {
    let info: (u32, u32, u32) = input.iter().fold((1024, 0, 0), |mut info, &n| {
        if n < info.0 {
            info.0 = n;
        }
        if n > info.1 {
            info.1 = n;
        }
        info.2 += n;
        info
    });

    (info.0..=info.1).sum::<u32>() - info.2
}

pub fn parse_input(input: &str) -> Vec<u32> {
    const B: u8 = b'B';
    const R: u8 = b'R';
    let mut output: Vec<u32> = Vec::with_capacity(1024);
    let bytes = input.as_bytes();
    let mut i: usize = 0;
    while i < bytes.len() {
        if (bytes[i] >> 4) != 0 {
            let mut value: u32 = 0;
            value |= ((bytes[i] == B) as u32) << 9;
            value |= ((bytes[i + 1] == B) as u32) << 8;
            value |= ((bytes[i + 2] == B) as u32) << 7;
            value |= ((bytes[i + 3] == B) as u32) << 6;
            value |= ((bytes[i + 4] == B) as u32) << 5;
            value |= ((bytes[i + 5] == B) as u32) << 4;
            value |= ((bytes[i + 6] == B) as u32) << 3;
            value |= ((bytes[i + 7] == R) as u32) << 2;
            value |= ((bytes[i + 8] == R) as u32) << 1;
            output.push(value | (bytes[i + 9] == R) as u32);
            i += 9;
        }
        i += 1
    }
    output
}

#[cfg(test)]
mod tests {
    use super::*;
    macro_rules! part_one_tests {
        ($($name:ident: $value:expr,)*) => {
        $(
            #[test]
            fn $name() {
                let (path, expected) = $value;
                let raw_input = fs::read_to_string(path).unwrap();
                let input: Vec<u32> = parse_input(&raw_input);
                assert_eq!(part_one(&input), expected);
            }
        )*
        }
    }

    macro_rules! part_two_tests {
        ($($name:ident: $value:expr,)*) => {
        $(
            #[test]
            fn $name() {
                let (path, expected) = $value;
                let raw_input = fs::read_to_string(path).unwrap();
                let input: Vec<u32> = parse_input(&raw_input);
                assert_eq!(part_two(&input), expected);
            }
        )*
        }
    }
    part_one_tests! {
        part_one_problem:   ("inputs/day05/problem.txt",    994),
        part_one_1:         ("inputs/day05/test01.txt",     820),
    }

    part_two_tests! {
        part_two_problem:   ("inputs/day05/problem.txt",    741),
    }
}
