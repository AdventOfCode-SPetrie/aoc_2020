use lazy_static::lazy_static;
use regex::Regex;
use std::collections::HashMap;
use std::fs;
use std::time::Instant;

pub fn exec() {
    let raw_input: String = fs::read_to_string("inputs/day04/problem.txt").unwrap();
    println!("Advent of Code: 2020-12-04");
    let start = Instant::now();
    let input: Vec<HashMap<&str, &str>> = parse_input(&raw_input);
    println!("P1: {}", part_one(&input));
    println!("P2: {}", part_two(&input));
    println!("Elapsed Time: {}ms\n", start.elapsed().as_millis());
}

pub fn part_one(input: &[HashMap<&str, &str>]) -> usize {
    input.iter().filter(|p| fields_present(p)).count()
}

pub fn part_two(input: &[HashMap<&str, &str>]) -> usize {
    input
        .iter()
        .filter(|p| fields_present(p))
        .filter(|p| validate_passport(p))
        .count()
}

fn fields_present(input: &HashMap<&str, &str>) -> bool {
    input.keys().filter(|&&k| k != "cid").count() >= 7
}

fn validate_passport(input: &HashMap<&str, &str>) -> bool {
    lazy_static! {
        static ref BYR_PATTERN: Regex = Regex::new("^19[2-9][0-9]$|^200[0-2]$").unwrap();
        static ref IYR_PATTERN: Regex = Regex::new("^(201[0-9]|2020)$").unwrap();
        static ref EYR_PATTERN: Regex = Regex::new("^(202[0-9]|2030)$").unwrap();
        static ref HGT_PATTERN: Regex =
            Regex::new("^((1[5-8][0-9]|19[0-3])cm|(59|6[0-9]|7[0-6])in)$").unwrap();
        static ref HCL_PATTERN: Regex = Regex::new("^(#[0-9a-f]{6})$").unwrap();
        static ref ECL_PATTERN: Regex = Regex::new("^(amb|blu|brn|gry|grn|hzl|oth)$").unwrap();
        static ref PID_PATTERN: Regex = Regex::new("^(\\d{9})$").unwrap();
    }

    BYR_PATTERN.is_match(input["byr"])
        && IYR_PATTERN.is_match(input["iyr"])
        && EYR_PATTERN.is_match(input["eyr"])
        && HGT_PATTERN.is_match(input["hgt"])
        && HCL_PATTERN.is_match(input["hcl"])
        && ECL_PATTERN.is_match(input["ecl"])
        && PID_PATTERN.is_match(input["pid"])
}

pub fn parse_input<'a>(input: &'a str) -> Vec<HashMap<&'a str, &'a str>> {
    input
        .split("\n\n")
        .flat_map(|s| s.split("\r\n\r\n"))
        .map(|e| {
            let mut passport = HashMap::<&str, &str>::new();
            e.trim().split(|p| p == ' ' || p == '\n').for_each(|x| {
                let mut tokens = x.trim().split(':');
                passport.insert(tokens.next().unwrap(), tokens.next().unwrap());
            });
            passport
        })
        .collect()
}

#[cfg(test)]
mod tests {
    use super::*;
    macro_rules! part_one_tests {
        ($($name:ident: $value:expr,)*) => {
        $(
            #[test]
            fn $name() {
                let (path, expected) = $value;
                let raw_input = fs::read_to_string(path).unwrap();
                let input = parse_input(&raw_input);
                assert_eq!(part_one(&input), expected);
            }
        )*
        }
    }
    macro_rules! part_two_tests {
        ($($name:ident: $value:expr,)*) => {
        $(
            #[test]
            fn $name() {
                let (path, expected) = $value;
                let raw_input = fs::read_to_string(path).unwrap();
                let input = parse_input(&raw_input);
                assert_eq!(part_two(&input), expected);
            }
        )*
        }
    }
    part_one_tests! {
        part_one_problem:   ("inputs/day04/problem.txt",    242),
        part_one_1:         ("inputs/day04/test01.txt",     2),
    }
    part_two_tests! {
        part_two_problem:   ("inputs/day04/problem.txt",    186),
    }
}
