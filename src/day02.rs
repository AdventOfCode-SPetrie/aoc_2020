use std::convert::TryFrom;
use std::fs;
use std::time::Instant;

pub fn exec() {
    let raw_input = fs::read_to_string("inputs/day02/problem.txt").unwrap();
    println!("Advent of Code: 2020-12-02");
    let start = Instant::now();
    let input: Vec<Row> = parse_input(&raw_input);
    println!("P1: {}", part_one(&input));
    println!("P2: {}", part_two(&input));
    println!("Elapsed Time: {}ms\n", start.elapsed().as_millis());
}

pub fn part_one(input: &[Row]) -> usize {
    input
        .iter()
        .filter(|v| {
            let count = bytecount::count(v.password, v.key);
            count >= v.min && count <= v.max
        })
        .count()
}

pub fn part_two(input: &[Row]) -> usize {
    input
        .iter()
        .filter(|v| (v.password[v.min - 1] == (v.key)) ^ (v.password[v.max - 1] == (v.key)))
        .count()
}

pub struct Row<'a> {
    min: usize,
    max: usize,
    key: u8,
    password: &'a [u8],
}

impl<'a> TryFrom<&'a str> for Row<'a> {
    type Error = std::num::ParseIntError;
    fn try_from(n: &'a str) -> Result<Self, Self::Error> {
        let mut tokens = n
            .split(|c| c == '-' || c == ' ' || c == ':')
            .filter(|&x| !x.is_empty());
        Ok(Row {
            min: match tokens.next() {
                Some(v) => v.parse()?,
                None => panic!("Min Token not Present: {}", n),
            },
            max: match tokens.next() {
                Some(v) => v.parse()?,
                None => panic!("Max Token not Present: {}", n),
            },
            key: match tokens.next() {
                Some(v) => v.as_bytes()[0],
                None => panic!("Key Token not Present: {}", n),
            },
            password: match tokens.next() {
                Some(v) => v.as_bytes(),
                None => panic!("Password Token not Present: {}", n),
            },
        })
    }
}

pub fn parse_input(input: &str) -> Vec<Row> {
    input.lines().map(|v| Row::try_from(v).unwrap()).collect()
}

#[cfg(test)]
mod tests {
    use super::*;
    macro_rules! part_one_tests {
        ($($name:ident: $value:expr,)*) => {
        $(
            #[test]
            fn $name() {
                let (path, expected) = $value;
                let raw_input = fs::read_to_string(path).unwrap();
                let input: Vec<Row> = parse_input(&raw_input);
                assert_eq!(part_one(&input), expected);
            }
        )*
        }
    }

    macro_rules! part_two_tests {
        ($($name:ident: $value:expr,)*) => {
        $(
            #[test]
            fn $name() {
                let (path, expected) = $value;
                let raw_input = fs::read_to_string(path).unwrap();
                let input: Vec<Row> = parse_input(&raw_input);
                assert_eq!(part_two(&input), expected);
            }
        )*
        }
    }
    part_one_tests! {
        part_one_problem:   ("inputs/day02/problem.txt",    548),
        part_one_1:         ("inputs/day02/test01.txt",     2),
    }

    part_two_tests! {
        part_two_problem:   ("inputs/day02/problem.txt",    502),
        part_two_1:         ("inputs/day02/test01.txt",     1),
    }
}
