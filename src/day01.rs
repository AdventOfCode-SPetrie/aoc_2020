use std::collections::HashMap;
use std::convert::TryInto;
use std::fs;
use std::time::Instant;

pub fn exec() {
    let raw_input: String = fs::read_to_string("inputs/day01/problem.txt").unwrap();
    println!("Advent of Code: 2020-12-01");
    let start = Instant::now();
    let input: Vec<i32> = parse_input(&raw_input);
    println!("P1: {}", part_one(&input));
    println!("P2: {}", part_two(&input));
    println!("Elapsed Time: {}ms\n", start.elapsed().as_millis());
}

pub fn part_one(input: &[i32]) -> i32 {
    let terms: (i32, i32) = find_two_sum_terms(input, 2020).unwrap();
    terms.0 * terms.1
}

pub fn part_two(input: &[i32]) -> i32 {
    let terms: (i32, i32, i32) = find_three_sum_terms(input, 2020).unwrap();
    terms.0 * terms.1 * terms.2
}

fn find_two_sum_terms(input: &[i32], target: i32) -> Result<(i32, i32), &'static str> {
    let mut numbers: HashMap<i32, u32> = HashMap::new();
    for v in input {
        numbers.entry(*v).and_modify(|e| *e += 1).or_insert(1);
    }
    for number in numbers.keys() {
        match numbers.get(&(target - number)) {
            Some(n) => {
                if *number == target - *number && *n == 1 {
                    continue;
                }
                return Ok((*number, target - *number));
            }
            None => continue,
        }
    }
    Err("No Two-Sum Found")
}

fn find_three_sum_terms(input: &[i32], target: i32) -> Result<(i32, i32, i32), &'static str> {
    let mut numbers: HashMap<i32, u32> = HashMap::new();
    for v in input {
        numbers.entry(*v).and_modify(|e| *e += 1).or_insert(1);
    }
    for i in 0..input.len() {
        for j in 1..input.len() {
            let value = target - input[i] - input[j];
            match numbers.get(&value) {
                Some(n) => {
                    if [input[i], input[j], value]
                        .iter()
                        .filter(|v| **v == value)
                        .count()
                        < (*n).try_into().unwrap()
                    {
                        continue;
                    }
                    return Ok((input[i], input[j], value));
                }
                None => continue,
            }
        }
    }
    Err("No Three-Sum Found")
}

pub fn parse_input(input: &str) -> Vec<i32> {
    input.lines().map(|v| v.parse().unwrap()).collect()
}

#[cfg(test)]
mod tests {
    use super::*;
    macro_rules! part_one_tests {
        ($($name:ident: $value:expr,)*) => {
        $(
            #[test]
            fn $name() {
                let (path, expected) = $value;
                let raw_input: String = fs::read_to_string(path).unwrap();
                let input = parse_input(&raw_input);
                assert_eq!(part_one(&input), expected);
            }
        )*
        }
    }

    macro_rules! part_two_tests {
        ($($name:ident: $value:expr,)*) => {
        $(
            #[test]
            fn $name() {
                let (path, expected) = $value;
                let raw_input: String = fs::read_to_string(path).unwrap();
                let input = parse_input(&raw_input);
                assert_eq!(part_two(&input), expected);
            }
        )*
        }
    }
    part_one_tests! {
        part_one_problem:   ("inputs/day01/problem.txt",    921504),
        part_one_1:         ("inputs/day01/test01.txt",     514579),
    }

    part_two_tests! {
        part_two_problem:   ("inputs/day01/problem.txt",    195700142),
        part_two_1:         ("inputs/day01/test01.txt",     241861950),
    }
}
