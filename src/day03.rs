use std::fs;
use std::time::Instant;

pub fn exec() {
    let raw_input: String = fs::read_to_string("inputs/day03/problem.txt").unwrap();
    println!("Advent of Code: 2020-12-03");
    let start = Instant::now();
    let input: Downhill = parse_input(&raw_input);
    println!("P1: {}", part_one(&input));
    println!("P2: {}", part_two(&input));
    println!("Elapsed Time: {}ms\n", start.elapsed().as_millis());
}

pub fn part_one(input: &Downhill) -> u64 {
    calculate_trees(&input, 3, 1)
}

pub fn part_two(input: &Downhill) -> u64 {
    calculate_trees(&input, 1, 1)
        * calculate_trees(&input, 3, 1)
        * calculate_trees(&input, 5, 1)
        * calculate_trees(&input, 7, 1)
        * calculate_trees(&input, 1, 2)
}

fn calculate_trees(input: &Downhill, right: usize, down: usize) -> u64 {
    let mut row: usize = 0;
    let mut col: usize = 0;
    let mut value: u64 = 0;

    while row < input.land.len() {
        if input.get(row, col) {
            value += 1;
        }
        row += down;
        col += right;
        if col >= input.size {
            col -= input.size;
        }
    }

    value
}

pub struct Downhill {
    size: usize,
    land: Vec<u32>,
}

impl Downhill {
    pub fn get(&self, row: usize, col: usize) -> bool {
        (self.land[row] & (1 << (col))) != 0
    }
}

pub fn parse_input(input: &str) -> Downhill {
    const TREE: u8 = b'#';
    Downhill {
        size: input.lines().next().unwrap().as_bytes().len(),
        land: input
            .lines()
            .map(|l| {
                l.as_bytes()
                    .iter()
                    .rev()
                    .fold(0u32, |acc, &byte| (acc << 1) + (byte == TREE) as u32)
            })
            .collect(),
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    macro_rules! part_one_tests {
        ($($name:ident: $value:expr,)*) => {
        $(
            #[test]
            fn $name() {
                let (path, expected) = $value;
                let raw_input: String = fs::read_to_string(path).unwrap();
                let input: Downhill = parse_input(&raw_input);
                assert_eq!(part_one(&input), expected);
            }
        )*
        }
    }

    macro_rules! part_two_tests {
        ($($name:ident: $value:expr,)*) => {
        $(
            #[test]
            fn $name() {
                let (path, expected) = $value;
                let raw_input: String = fs::read_to_string(path).unwrap();
                let input: Downhill = parse_input(&raw_input);
                assert_eq!(part_two(&input), expected);
            }
        )*
        }
    }
    part_one_tests! {
        part_one_problem:   ("inputs/day03/problem.txt",    289),
        part_one_1:         ("inputs/day03/test01.txt",     7),
    }

    part_two_tests! {
        part_two_problem:   ("inputs/day03/problem.txt",    5522401584),
        part_two_1:         ("inputs/day03/test01.txt",     336),
    }
}
