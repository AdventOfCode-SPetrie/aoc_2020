use std::time::Instant;

mod day01;
mod day02;
mod day03;
mod day04;
mod day05;
mod day06;
//mod day07;
mod day08;
mod day09;

fn main() {
    let start = Instant::now();
    day01::exec();
    day02::exec();
    day03::exec();
    day04::exec();
    day05::exec();
    day06::exec();
    //day07::exec();
    day08::exec();
    day09::exec();
    println!(
        "Advent of Code 2020 Total Time: {}ms\n",
        start.elapsed().as_millis()
    );
}
