use std::convert::TryFrom;
use std::fs;
use std::time::Instant;

pub fn exec() {
    let raw_input = fs::read_to_string("inputs/day08/problem.txt").unwrap();
    println!("Advent of Code: 2020-12-08");
    let start = Instant::now();
    let input: Vec<Instruction> = parse_input(&raw_input);
    //input.iter().for_each(|x| println!("{:?}", x));
    println!("P1: {}", part_one(&input));
    println!("P2: {}", part_two(&input));
    println!("Elapsed Time: {}ms\n", start.elapsed().as_millis());
}

pub fn part_one(input: &[Instruction]) -> i32 {
    let mut acc: i32 = 0;
    let mut instruction_pointer: usize = 0;
    let mut visited: Vec<bool> = vec![false; input.len()];
    while !visited[instruction_pointer] {
        visited[instruction_pointer] = true;
        match input[instruction_pointer] {
            Instruction::ACC(v) => {
                acc += v;
                instruction_pointer += 1;
            }
            Instruction::JMP(v) => {
                instruction_pointer = ((instruction_pointer as i32) + v) as usize;
            }
            Instruction::NOP(_) => {
                instruction_pointer += 1;
            }
        }
    }
    acc
}

pub fn part_two(input: &[Instruction]) -> i32 {
    let mut acc: i32 = 0;
    let mut instruction_pointer: usize = 0;
    let mut visited: Vec<bool> = vec![false; input.len()];
    let mut potentials: Vec<(usize, i32)> = Vec::with_capacity(200);
    let mut switch = false;
    let mut global_switch: bool = true;
    while instruction_pointer < input.len() {
        while instruction_pointer < input.len() && !visited[instruction_pointer] {
            visited[instruction_pointer] = true;
            match input[instruction_pointer] {
                Instruction::ACC(v) => {
                    acc += v;
                    instruction_pointer += 1;
                }
                Instruction::JMP(v) => {
                    if switch {
                        switch = false;
                        instruction_pointer += 1;
                    } else {
                        if global_switch {
                            potentials.push((instruction_pointer, acc));
                        }
                        instruction_pointer = ((instruction_pointer as i32) + v) as usize;
                    }
                }
                Instruction::NOP(v) => {
                    if switch {
                        switch = false;
                        instruction_pointer = ((instruction_pointer as i32) + v) as usize;
                    } else {
                        if global_switch {
                            potentials.push((instruction_pointer, acc));
                        }
                        instruction_pointer += 1;
                    }
                }
            }
        }
        if instruction_pointer < input.len() {
            let reset = potentials.pop().unwrap();
            instruction_pointer = reset.0;
            acc = reset.1;
            visited[instruction_pointer] = false;
            switch = true;
            global_switch = false;
        }
    }
    acc
}

#[derive(Debug)]
pub enum Instruction {
    ACC(i32),
    JMP(i32),
    NOP(i32),
}

impl TryFrom<&[u8]> for Instruction {
    type Error = &'static str;
    fn try_from(bytes: &[u8]) -> Result<Self, Self::Error> {
        let value: i32 = if bytes[4] == b'-' {
            bytes[5..]
                .iter()
                .fold(0i32, |acc, b| acc * 10 + (b - b'0') as i32)
                .wrapping_neg()
        } else {
            bytes[5..]
                .iter()
                .fold(0i32, |acc, b| acc * 10 + (b - b'0') as i32)
        };
        match bytes[0] {
            b'a' => Ok(Instruction::ACC(value)),
            b'j' => Ok(Instruction::JMP(value)),
            b'n' => Ok(Instruction::NOP(value)),
            _ => Err("Invalid Input"),
        }
    }
}

pub fn parse_input(input: &str) -> Vec<Instruction> {
    let mut instructions: Vec<Instruction> = Vec::with_capacity(1024);

    let bytes = input.as_bytes();
    let mut start = 0;
    let mut index: usize = 0;
    while index < bytes.len() {
        if bytes[index] == b'\n' {
            instructions.push(Instruction::try_from(&bytes[start..index]).unwrap());
            index += 1;
            start = index;
        } else if bytes[index] == b'\r' {
            instructions.push(Instruction::try_from(&bytes[start..index]).unwrap());
            index += 2;
            start = index;
        } else {
            index += 1;
        }
    }
    instructions.push(Instruction::try_from(&bytes[start..index]).unwrap());
    instructions
}

#[cfg(test)]
mod tests {
    use super::*;
    macro_rules! part_one_tests {
        ($($name:ident: $value:expr,)*) => {
        $(
            #[test]
            fn $name() {
                let (path, expected) = $value;
                let raw_input = fs::read_to_string(path).unwrap();
                let input = parse_input(&raw_input);
                assert_eq!(part_one(&input), expected);
            }
        )*
        }
    }

    macro_rules! part_two_tests {
        ($($name:ident: $value:expr,)*) => {
        $(
            #[test]
            fn $name() {
                let (path, expected) = $value;
                let raw_input = fs::read_to_string(path).unwrap();
                let input = parse_input(&raw_input);
                assert_eq!(part_two(&input), expected);
            }
        )*
        }
    }
    part_one_tests! {
        part_one_problem:   ("inputs/day08/problem.txt",    1087),
        part_one_1:         ("inputs/day08/test01.txt",     5),
    }

    part_two_tests! {
        part_two_problem:   ("inputs/day08/problem.txt",    780),
        part_two_1:         ("inputs/day08/test01.txt",     8),
    }
}
