use lazy_static::lazy_static;
use petgraph::Graph;
use regex::Regex;
use std::fs;
use std::time::Instant;

pub fn _exec() {
    let _raw_input = fs::read_to_string("inputs/day07/test01.txt").unwrap();
    println!("Advent of Code: 2020-12-07");
    let start = Instant::now();
    //parse_input(&raw_input);
    //let input: (u64, u64) = parse_input(&raw_input);
    //println!("P1: {}", part_one(&input));
    //println!("P2: {}", part_two(&input));
    println!("Elapsed Time: {}ms\n", start.elapsed().as_millis());
}

pub fn _part_one(input: &(u64, u64)) -> u64 {
    input.0
}

pub fn _part_two(input: &(u64, u64)) -> u64 {
    input.1
}

pub fn _parse_input<'a>(input: &'a str) {
    lazy_static! {
        static ref PARENT: Regex = Regex::new("((?:\\w+\\s*)+) bags contain").unwrap();
        static ref CHILD: Regex = Regex::new("(\\d+) ((?:\\w+\\s*)+) bag[s]{0,1}[,\\.]").unwrap();
    }

    println!("{}, {}",&input[0..9],  &input[0..9] == "light red");
    let mut index: usize = 0;
    let mut slice_start: usize = 0;
    let mut parent_found: bool = false;
    let mut space_count: u8 = 0;
    let bytes = input.as_bytes();
    while index < bytes.len() {
        if bytes[index] == b'\n' {
            index += 2;
            parent_found = false;
        } else if bytes[index] == b'\r' {
            index += 4;
            parent_found = false;
        } else if bytes[index] == b' ' {
            index += 1;
        }
    }
    /*
    let mut bag_graph = Graph::<&str, u32>::new();
    input.lines().for_each(|x| {
        let parent = &PARENT.captures(x).unwrap()[1];
        //let parent_node = bag_graph.add_node(&parent);
        for m in CHILD.captures_iter(x) {
            //let child_node = bag_graph.add_node(&m[2].to_owned());
            //bag_graph.add_edge(parent_node, child_node, *(&m[1].parse().unwrap()));
            println!("{} - {}:{}",parent, &m[1], &m[2]);
        }
        println!();
    });
    println!();
    */
}

pub fn _parse_input_regex<'a>(input: &'a str) {
    lazy_static! {
        static ref PARENT: Regex = Regex::new("((?:\\w+\\s*)+) bags contain").unwrap();
        static ref CHILD: Regex = Regex::new("(\\d+) ((?:\\w+\\s*)+) bag[s]{0,1}[,\\.]").unwrap();
    }
    let mut bag_graph = Graph::<&str, u32>::new();
    input.lines().for_each(|x| {
        let parent = &PARENT.captures(x).unwrap()[1];
        //let parent_node = bag_graph.add_node(&parent);
        for m in CHILD.captures_iter(x) {
            //let child_node = bag_graph.add_node(&m[2].to_owned());
            //bag_graph.add_edge(parent_node, child_node, *(&m[1].parse().unwrap()));
            println!("{} - {}:{}",parent, &m[1], &m[2]);
        }
        println!();
    });
    println!();
}

#[cfg(test)]
mod tests {
    use super::*;
    macro_rules! part_one_tests {
        ($($name:ident: $value:expr,)*) => {
        $(
            #[test]
            fn $name() {
                let (path, expected) = $value;
                let raw_input = fs::read_to_string(path).unwrap();
                let input = parse_input(&raw_input);
                assert_eq!(part_one(&input), expected);
            }
        )*
        }
    }

    macro_rules! part_two_tests {
        ($($name:ident: $value:expr,)*) => {
        $(
            #[test]
            fn $name() {
                let (path, expected) = $value;
                let raw_input = fs::read_to_string(path).unwrap();
                let input = parse_input(&raw_input);
                assert_eq!(part_two(&input), expected);
            }
        )*
        }
    }
    part_one_tests! {
        //part_one_problem:   ("inputs/day06/problem.txt",    208),
        //part_one_1:         ("inputs/day07/test01.txt",     4),
    }

    part_two_tests! {
        //part_two_problem:   ("inputs/day06/problem.txt",    1664),
        //part_two_1:         ("inputs/day06/test02.txt",     126),
    }
}
